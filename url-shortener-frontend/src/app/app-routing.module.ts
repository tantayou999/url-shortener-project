import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LinkComponent } from './link/link.component';
import { BannerComponent } from './banner/banner.component';


const routes: Routes = [
  { path: '', component: BannerComponent },
  { path: ':id', component: LinkComponent }
];
// const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
