import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const BASE_URL = window.location.origin
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  urls = []
  new_url : string = '';
  error = false;
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  shorten(){
    if(this.validURL(this.new_url)){
      this.error = false;
      this.http.post<any>('http://urlshortenerbackend-env-1.xequcjjb3z.us-east-1.elasticbeanstalk.com/shorten', 
      { url: this.new_url }).subscribe({
        next: data => { this.urls.push({long: this.new_url, short: data.url}); 
        this.new_url = '';},
        error: error => console.error('There was an error!', error)
      })
      if(this.urls.length > 3){
        this.urls.shift()
    }} else {
      this.error = true;
    }
  }

  copy(url){
    this.copyMessage(BASE_URL+"/"+url.short)
  }

  copyMessage(val: string){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }
}
