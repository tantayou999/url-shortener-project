import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router"

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.css']
})
export class LinkComponent implements OnInit {

  constructor(private http: HttpClient,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {      
    const id = this.route.snapshot.paramMap.get('id');
    this.http.get<any>('http://urlshortenerbackend-env-1.xequcjjb3z.us-east-1.elasticbeanstalk.com/get/'+id).subscribe({
    next: data => { 
      var prefix = 'http://';
      var url = data.url
      if (url.substr(0, prefix.length) !== prefix)
      {
        url = prefix + url;
      }
      window.location.assign(url) },
    error: error => this.router.navigate([''])
  })

  }

}
