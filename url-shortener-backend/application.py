from flask import Flask, jsonify, abort, request
from flask_cors import CORS
import mysql.connector
import base64
import os
application = Flask(__name__)
CORS(application)

DB_USER = 'admin'
DB_PASSWORD = '123qwe!Q'
DB_HOST = 'database-url-shortener.cmfwx9juoktd.us-east-1.rds.amazonaws.com'
DB_NAME = 'innodb'

@application.route('/shorten', methods=['POST'])
def add_url():
    if not request.json or not 'url' in request.json:
        abort(400)
    cnx = mysql.connector.connect(user=DB_USER, password=DB_PASSWORD,
                                host=DB_HOST,
                                database=DB_NAME)
    cursor = cnx.cursor()
    query = ("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'innodb' AND TABLE_NAME = 'urls'")
    cursor.execute(query)
    next_no = cursor.fetchall()[0][0]
    urlSafeEncodedBytes = base64.urlsafe_b64encode(str(next_no).encode("utf-8"))
    urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
    urlSafeEncodedStr = urlSafeEncodedStr.replace("=", "")
    add_url_query = ("INSERT INTO urls "
                "(short_url, long_url) "
                "VALUES (%s, %s)")
    cursor.execute(add_url_query, (urlSafeEncodedStr, request.json['url']))
    cnx.commit()
    cursor.close()
    cnx.close()

    return jsonify({'url': urlSafeEncodedStr})

@application.route('/get/<url_id>', methods=['GET'])
def get_url(url_id):    
    cnx = mysql.connector.connect(user=DB_USER, password=DB_PASSWORD,
                                host=DB_HOST,
                                database=DB_NAME)
    cursor = cnx.cursor()
    query = ("SELECT long_url FROM urls WHERE short_url = %s")
    cursor.execute(query, (url_id,))
    result = cursor.fetchall()
    cursor.close()
    cnx.close()
    if len(result) > 0:
        return jsonify({'url': result[0][0]})
    else:
        abort(404)

@application.route('/')
def hello_world():
    message = "Hello, world!"
    return jsonify({'url': message})

if __name__ == '__main__':
    application.run(host='0.0.0.0')